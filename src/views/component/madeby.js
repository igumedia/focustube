var m = require('mithril');

module.exports = {
    view: function(){
        return m('.madeby', [
            m('.madeby__wrapper', [
                m('span.madeby__text', 'Made with'),
                m('span.madeby__love', ' ♥ '),
                m('span.madeby__text', 'by'),
                m('a.madeby__link', {href:'https://twitter.com/MIGumelar'} ,' Irfan Gumelar'),
                m('span.madeby__text', ' in Indonesia'),
            ])
        ])
    }
}