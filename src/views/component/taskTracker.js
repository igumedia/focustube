var m = require('mithril');
var taskModel =require('../../model/taskModel').Task;
var dateformat = require("dateformat")
const prettyMilliseconds = require('pretty-ms');

module.exports = {
    oninit: function(){
        taskModel.initialLoad()
    },
    view: function(){
        return m('div.task.search-filter',
        [
            m('form.task-form',{
                onsubmit: function(e){
                    e.preventDefault();
                    taskModel.submitTask()
                }
            },
                [
                    m('div.task-form__toggle.fas.fa-flag-checkered', {
                        onclick: function(){
                            taskModel.toggleHideBrowser()
                        }
                    }),
                    m('input.task-form__input[type="text"][placeholder="What is your goal for today?"]', {
                        value: taskModel.taskInput,
                        required: true,
                        onchange: function(e){
                            taskModel.setTaskInput(e.target.value)
                        }
                    }),
                    m('button.task-form__submit',{
                        type: 'submit'
                    },
                        'Add task'
                    )
                ]
            ),
            m('div.task-browser',{
                class: taskModel.hideBrowser ? 'task-browser--hide' : null
            },
                [
                    m('div.task-browser__tabs',{
                        class: taskModel.taskBag.length ? null : 'task-browser__tabs--hide'
                    },
                        [
                            m('div.task-browser__tabs-header',{
                                class: taskModel.taskView === 'all' ? 'task-browser__tabs-header--active' : null,
                                onclick: function(){
                                    taskModel.setTaskView('all')
                                }
                            }, 
                                'All'
                            ),
                            m('div.task-browser__tabs-header',{
                                class: taskModel.taskView === false ? 'task-browser__tabs-header--active' : null,
                                onclick: function(){
                                    taskModel.setTaskView(false)
                                }
                            },
                                'To-do'
                            ),
                            m('div.task-browser__tabs-header',{
                                class: taskModel.taskView === true ? 'task-browser__tabs-header--active' : null,
                                onclick: function(){
                                    taskModel.setTaskView(true)
                                }
                            }, 
                                'Done'
                            )
                        ]
                    ),
                    m('div.task-browser__items',
                        taskModel.taskBag.map(function(task){
                            
                            if(taskModel.taskView === task.completed || taskModel.taskView === 'all' ){
                                return m('div.task-browser__item', {key: task.taskId},
                                        [
                                            m('input.task-browser__item-checkbox[type="checkbox"]',{
                                                checked: task.completed === true ? true : false,
                                                onchange: function(){
                                                    taskModel.setCompleted(task)
                                                }
                                            }),
                                            m('div.task-browser__item-description',
                                                [
                                                    m('div.task-browser__item-description__todo',{
                                                        class: task.completed === true ? 'task-browser__item-description__todo--active' : null,
                                                        onclick: function(){
                                                            taskModel.setExpandDetails(task)
                                                        }
                                                    }, 
                                                        task.taskDesc
                                                    ),
                                                    m('div.task-browser__item-description__meta',{
                                                        class : task.expandDetails === false ? 'task-browser__item-description__meta--hide' : null
                                                    },
                                                        [
                                                            m('div.task-browser__item-description__meta-wrapper',
                                                                [
                                                                    m('div.task-browser__item-description__meta-item',
                                                                        [
                                                                            m('div.task-browser__item-description__meta-item__tittle', 
                                                                                'Created at :'
                                                                            ),
                                                                            m('div.task-browser__item-description__meta-item__value', 
                                                                                dateformat(task.createdAt, 'dd mmm yy, hh:MMTT')
                                                                            )
                                                                        ]
                                                                    ),
                                                                    m('div.task-browser__item-description__meta-item',
                                                                        [
                                                                            m('div.task-browser__item-description__meta-item__tittle', 
                                                                                'Completed at :'
                                                                            ),
                                                                            m('div.task-browser__item-description__meta-item__value', 
                                                                                task.completedAt ? dateformat(task.completedAt, 'dd mmm yy, hh:MMTT') : 'Pending'
                                                                            )
                                                                        ]
                                                                    ),
                                                                    m('div.task-browser__item-description__meta-item',
                                                                        [
                                                                            m('div.task-browser__item-description__meta-item__tittle', 
                                                                                'Time to completion :'
                                                                            ),
                                                                            m('div.task-browser__item-description__meta-item__value', 
                                                                                task.TTC ? prettyMilliseconds(task.TTC)  : 'On Progress'
                                                                            )
                                                                        ]
                                                                    )
                                                                ]
                                                            ),
                                                            m('div.task-browser__item-description__meta-delete',{
                                                                onclick: function(){
                                                                    if(window.confirm("Do you really want to remove this task")){
                                                                        taskModel.removeTask(task.taskId)
                                                                    }
                                                                }
                                                            }, 
                                                                ' [x] remove task '
                                                            )
                                                        ]
                                                    )
                                                ]
                                            ),
                                            m('div.task-browser__item-status.fas.fa-check',{
                                                class: task.completed === true ? 'task-browser__item-status--active' : null,
                                            })
                                        ]
                                    )
                            }
                            else{
                                return m.fragment({key: task.taskId}, [])
                            }

                        })

                        // [
                        //     m('div.task-browser__item',
                        //         [
                        //             m('input.task-browser__item-checkbox[type="checkbox"]'),
                        //             m('div.task-browser__item-description',
                        //                 [
                        //                     m('div.task-browser__item-description__todo.task-browser__item-description__todo--active', 
                        //                         ' Fix iframe layout, put task component above it '
                        //                     ),
                        //                     m('div.task-browser__item-description__meta.',
                        //                         [
                        //                             m('div.task-browser__item-description__meta-wrapper',
                        //                                 [
                        //                                     m('div.task-browser__item-description__meta-item',
                        //                                         [
                        //                                             m('div.task-browser__item-description__meta-item__tittle', 
                        //                                                 'Created at :'
                        //                                             ),
                        //                                             m('div.task-browser__item-description__meta-item__value', 
                        //                                                 '12 Nov 19, 14:15PM'
                        //                                             )
                        //                                         ]
                        //                                     ),
                        //                                     m('div.task-browser__item-description__meta-item',
                        //                                         [
                        //                                             m('div.task-browser__item-description__meta-item__tittle', 
                        //                                                 'Created at :'
                        //                                             ),
                        //                                             m('div.task-browser__item-description__meta-item__value', 
                        //                                                 '12 Nov 19, 14:15PM'
                        //                                             )
                        //                                         ]
                        //                                     ),
                        //                                     m('div.task-browser__item-description__meta-item',
                        //                                         [
                        //                                             m('div.task-browser__item-description__meta-item__tittle', 
                        //                                                 'Created at :'
                        //                                             ),
                        //                                             m('div.task-browser__item-description__meta-item__value', 
                        //                                                 '12 Nov 19, 14:15PM'
                        //                                             )
                        //                                         ]
                        //                                     )
                        //                                 ]
                        //                             ),
                        //                             m('div.task-browser__item-description__meta-delete', 
                        //                                 ' [x] remove task '
                        //                             )
                        //                         ]
                        //                     )
                        //                 ]
                        //             ),
                        //             m('div.task-browser__item-status.fas.fa-check.task-browser__item-status--active')
                        //         ]
                        //     )
                        // ]
                    )
                ]
            )
        ]
    )
    }
}