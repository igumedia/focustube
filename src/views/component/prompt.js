var m = require('mithril');
var promptModel = require('../../model/promptModel').Prompt;

module.exports = {
    onupdate: function(){
        document.getElementById('input_prompt').focus()
    },
    view: function(){
        return m('.prompt', {class: promptModel.showPrompt === true ? null : 'prompt--hide' }, [
            m('form.prompt-box', {
                onsubmit: function(e){
                   promptModel.submitPrompt(e)
                }
            },[
                m('.prompt-box__header', promptModel.headerPrompt),
                m('.prompt-box__message', [
                    m('.prompt-box__message-description', promptModel.descriptionPrompt),
                    m('.prompt-box__message-input', [
                        m('input.prompt-box__input', {
                            id : 'input_prompt',
                            required: true,
                            type: 'text',
                            value: promptModel.valueInputPrompt,
                            oninput: function(e){
                                promptModel.setInput(e.target.value)
                            }
                        })
                    ])
                ]),
                m('.prompt-box__action', [
                    m('div.prompt-box__action-button.prompt-box__action-button--cancel',{
                        onclick: function(){
                            promptModel.resetPrompt()
                        }
                    }, 'Cancel'),
                    m('button.prompt-box__action-button.prompt-box__action-button--ok', {type:'submit'}, 'Ok')
                ])
            ])
            
        ])
    }
}