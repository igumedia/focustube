var m = require('mithril');
var searchModel = require('../../model/searchModel').Search;
var taskModel = require('../../model/taskModel').Task;

module.exports = {
    view: function(){
        return m('.topbar', [
            m( m.route.Link, {selector: 'a.topbar__logo', href: '/'}, [
                m('span.logo__focus.logo__focus--medium', 'focus'),
                m('span.logo__tube.logo__tube--medium', 'hub')
            ]),
            m('form.topbar__search', {
                onsubmit: function(e){
                    searchModel.redirectSearch(e, '');
                }
            }, [
                m('input.topbar__search-input', {
                    value: searchModel.keyword, 
                    disabled: taskModel.taskBag.length ? false : true,
                    title: taskModel.taskBag.length ? 'Please fill this field' : 'Please set your goal first',
                    oninput: function(e){
                        searchModel.setKeyword(e.target.value)
                    },
                    required: true}),
                m('button.topbar__search-button', {type: 'submit'}, [
                    m('i.fas.fa-search')
                ])
            ]),
            m('.topbar__panel', [
                m('a.topbar__panel-item__donate-wrapper', {href: 'https://www.buymeacoffee.com/migumelar'}, [
                    m('img.topbar__panel-item__support', {src: '/img/buymecoffee.png'})
                ])
            ])
        ])
    }
}