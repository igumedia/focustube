var m = require('mithril');
var videoItemView = require('./videoItem');
var playlistModel = require('../../model/playlistModel').Playlist;
var promptModel = require('../../model/promptModel').Prompt;

module.exports = {
    getPlaylistClass: function(vnode){
        var playlistClass

        if(vnode.attrs.watchRoute){
            playlistClass = 'playlist__watch watch-media__playlist__content'
        }

        if(playlistModel.showBody === false){
            playlistClass = playlistClass + ' playlist--hide-body'
        }

        return playlistClass
    },
    getBodyClass: function(vnode){
        var bodyClass

        if(vnode.attrs.watchRoute){
            bodyClass = 'playlist-body__watch'
        }

        if(playlistModel.showBody === false){
            bodyClass = bodyClass + ' playlist-body--hidea'
        }

        return bodyClass
    },
    oncreate: function(){
        var windowSize = window.matchMedia("(max-width: 850px)")

        if (windowSize.matches) { // If media query matches
            playlistModel.toggleBody()
        }
    },
    oninit: function(){
        playlistModel.initialLoad()
        // console.log('triggered')
    },
    view: function(vnode){
        return m('.playlist', {class: this.getPlaylistClass(vnode)}, [
            m('.playlist-header', {
                onclick: function(){
                    var windowSize = window.matchMedia("(max-width: 850px)")

                    if (windowSize.matches) { // If media query matches
                        playlistModel.toggleBody()
                    }
                }
            }, [
                m('.playlist-header__tittle', 'FocusHub Playlist'),
                m('.playlist-header__toggle', [
                    m('i.fas.fa-angle-down')
                ])
            ]),
            m('.playlist-control ', {class: playlistModel.showBody ? null : 'playlist-control--hide'}, [
                m('.playlist-control__button', [
                    m('.playlist-control__button-presentation', [
                        m('i.playlist-control__button-item.fas.fa-th-large', {
                            title: 'Show thumbnail',
                            class: playlistModel.thumbnailMode ? 'playlist-control__button-item--active' : null,
                            onclick: playlistModel.toggleMode
                        }),
                        m('i.playlist-control__button-item.fas.fa-th-list', {
                            title: 'Hide thumbnail',
                            class: playlistModel.textMode ? 'playlist-control__button-item--active' : null,
                            onclick: playlistModel.toggleMode
                         })
                    ]),
                    m('.playlist-control__button-action', [
                        playlistModel.activePlaylist === 'temporary' ?
                        m('i.playlist-control__button-item.fas.fa-save', {
                            title: 'Save playlist',
                            onclick: function(){
                                promptModel.togglePrompt('Save new playlist', 'Enter a name for your new playlist', true, playlistModel.saveNewPlaylist)
                            }
                        }) : null,
                        // m('i.playlist-control__button-item.fas.fa-retweet'),
                        m('i.playlist-control__button-item.fas.fa-random', {
                            title: 'Shuffle play',
                            class: playlistModel.shuffle ? 'playlist-control__button-item--active' : null,
                            onclick: function(){
                                playlistModel.toggleShuffle()
                            }
                        })
                    ])
                ]),
                m('select.playlist-control__ownedList',  { 
                    name: 'active_playlist',
                    onchange: function(e){
                        playlistModel.setActivePlaylist(e.target.value)
                        // console.log(e.target.value)
                    }
                }, [
                    m('option', {
                        // key: 'temporary',
                        value: 'temporary',
                        selected: playlistModel.activePlaylist === 'temporary' ? true : null
                    }, 'New Playlist'),
                    Object.keys(playlistModel.list).map(function(keyObj,index){
                        return m('option', {
                            key: keyObj, 
                            value: keyObj,
                            selected: playlistModel.activePlaylist === keyObj ? true : null
                        }, playlistModel.list[keyObj].playlist_name)
                    })
                ])
            ]),
            m('.playlist-body',{class: this.getBodyClass(vnode)} , [
                playlistModel.activePlaylist === 'temporary' ?
                playlistModel.temporary.map(function(video){
                    return m(videoItemView, {playlist: true, key: video.uniqId , video: video})
                }) :
                playlistModel.list[playlistModel.activePlaylist].playlist_videos.map(function(video){
                    return m(videoItemView, {playlist: true, key: video.uniqId , video: video})
                })
                

            ]),
            m('.playlist-delete',{class: playlistModel.showBody ? null : 'playlist-body--hide'}  , [
                m('.playlist-delete__text',{
                    title: 'Delete playlist',
                    onclick: function(){
                        if(window.confirm("Do you really want to permanently delete this playlist?")){
                            playlistModel.deletePlaylist()
                        }
                    }
                } , '[x] delete playlist')
            ])

        ])
    }
}