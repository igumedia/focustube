var m = require('mithril');


module.exports = {
    view: function(vnode){
        return m('.footer', [
            m('.footer-menu', [
                m('.footer__one', [
                    m(m.route.Link, {href: '/', class: 'footer__item'}, 'Home'),
                    m(m.route.Link, {href: '/about', class: 'footer__item'}, 'About'),
                    m(m.route.Link, {href: '/contact', class: 'footer__item'}, 'Contact'),
                    // m(m.route.Link, {href: '/about', class: 'footer__item'}, 'Contact Me'),
                    m(m.route.Link, {href: '/terms-and-policy', class: 'footer__item'}, 'Terms and Policy'),
                    // m(m.route.Link, {href: '/something', class: 'footer__item'}, 'Privacy'),
                ]),
                m('.footer__two', [
                    // m(m.route.Link, {href: '/something', class: 'footer__item'}, 'Terms'),
                    // m(m.route.Link, {href: '/something', class: 'footer__item'}, 'Privacy'),
                    m('a', {href: 'https://youtube.com'}, [
                        m('img.footer__developed-with-youtube', {src: '/img/developed-with-youtube-sentence-case-dark-small.png'})
                    ])
                ])
            ]),
            m('.footer-disc', [
                m('a.footer-disc__item', {href: 'https://icons8.com/icon/70810/sort-down'},('Sort Down icon by Icons8'), m('a.footer-disc__item', {href: 'https://fontawesome.com'},('| Font icon by FontAwesome'))),
                m('div','Focushub is not affliated or endorsed by YouTube'),
            ])
        ])
    }
}