var m = require('mithril');


module.exports = {
    view: function(vnode){
        return m('main', {class: 'wrapper'},[
            vnode.children
        ])
    }
}