var m = require('mithril');

module.exports = {
    view: function(){
        return m('.donation', [
            m('.donation__text', 'You think FocusHub is helpful? support me by'),
            m('a', {href: 'https://www.buymeacoffee.com/migumelar', class:'donation__button'}, [
                m('img', {src: 'https://www.buymeacoffee.com/assets/img/guidelines/download-assets-sm-1.svg' }, 'Buy me a coffee')
            ])
        ])
    }
}