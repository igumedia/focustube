var m = require('mithril');
var YTPlayer = require('yt-player')
var playerModel = require('../../model/playerModel').Player

module.exports = {
    oncreate: function(){
        // destroy and recreate player instance
        if(!playerModel.playerInstance){
            // if instance not exist create instance
            playerModel.createInstance() 
        }
        else{
            // if instance exist then destroy and create instance
            playerModel.playerInstance.destroy()
            playerModel.createInstance() 
        }
        
        // play video according videoId on URL
        playerModel.playerInstance.load(m.route.param().videoId, true)
        
        // set state of currently playing video
        playerModel.activeVideo = m.route.param().videoId

        // set up next video played when video ended
        playerModel.playerInstance.on('ended', function(){
            // redirect to next video
            playerModel.nextVideo()
        })
    },
    view: function(vnode){
        return m('.watch-media__iframe', {
            id: 'yt_player',
            enablejsapi: true,
            frameborder: '0', 
            allow:'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
        })
    }
}