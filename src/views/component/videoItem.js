var m = require('mithril');
var searchModel = require('../../model/searchModel').Search;
var playlistModel = require('../../model/playlistModel').Playlist;
var approx = require('approximate-number');
var timeAgo = require('time-ago');
var timeFormat = require('hh-mm-ss');

module.exports = {
    getClassThumbnail: function (playlist) {
        if (playlist) {
            //playlist result
            var classNameToggle = 'video-item__thumbnail-wrapper--playlist video-item__thumbnail-wrapper--playlist---hide'
            if (playlistModel.textMode) {
                classNameToggle = classNameToggle + ' video-item__thumbnail-wrapper--hide'
                return classNameToggle
            } else {
                return classNameToggle
            }
        } else {
            // main result
            if (searchModel.showThumbnail === false) {
                return 'video-item__thumbnail-wrapper--hide'
            }
        }
    },
    getClassTittle: function (playlist) {
        if (playlist) {
            return 'video-item-tittle--playlist'
        } else {
            if (searchModel.showThumbnail === false) {
                return 'video-item-tittle--textMode'
            }
        }
    },
    getActivePlaylistClass: function (playlist, videoId) {
        var activeClass
        if (playlist) {
            activeClass = 'video-item--playlist';

            if (m.route.param().videoId === videoId) {
                activeClass = activeClass + ' video-item--playlist---active'
            }
        }

        return activeClass

    },
    convertYoutubeDuration: function (yt_duration) {
        var time_extractor = /([0-9]*H)?([0-9]*M)?([0-9]*S)?$/;
        var extracted = time_extractor.exec(yt_duration);
        var hours = parseInt(extracted[1], 10) || 0;
        var minutes = parseInt(extracted[2], 10) || 0;
        var seconds = parseInt(extracted[3], 10) || 0;
        return (hours * 3600 * 1000) + (minutes * 60 * 1000) + (seconds * 1000);
    },
    view: function (vnode) {
        return m('.video-item', { class: vnode.attrs.playlist ? this.getActivePlaylistClass(vnode.attrs.playlist, vnode.attrs.video.videoId) : null }, [
            m(m.route.Link, {
                href: vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : '/watch/search/' + vnode.attrs.video.videoId,
                selector: 'a.video-item__thumbnail-wrapper',
                class: this.getClassThumbnail(vnode.attrs.playlist)
            }, [
                m('img.video-item-thumbnail', { src: vnode.attrs.video.thumbnail }),
                m('.video-item-duration', { class: vnode.attrs.playlist ? 'video-item-duration--playlist' : null }, timeFormat.fromMs(this.convertYoutubeDuration(vnode.attrs.video.duration)))
            ]),
            m('.video-item-meta', [
                m('.video-item__title-wrapper', [
                    m(m.route.Link, { href: vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : '/watch/search/' + vnode.attrs.video.videoId, selector: 'a.video-item-tittle', class: this.getClassTittle(vnode.attrs.playlist) }, m.trust(vnode.attrs.video.tittle)),
                    vnode.attrs.playlist ?
                        m('.video-item-togglePlaylist.video-item-togglePlaylist--playlist', [
                            m('i.fas.fa-times', {
                                title: 'Remove video from playlist',
                                onclick: function () {
                                    playlistModel.removePlaylistVideo(vnode.attrs.video.uniqId)
                                }
                            })
                        ]) :
                        m('.video-item-togglePlaylist', [
                            m('i.fas.fa-plus-square', {
                                title: 'Add to FocusHub Playlist',
                                onclick: function () {
                                    playlistModel.insertPlaylistVideo(vnode.attrs.video)
                                    // alert(vnode.attrs.video)
                                }
                            })
                        ])
                ]),
                m('.video-item__stat-wrapper', {
                    class: vnode.attrs.playlist ? 'video-item__stat-wrapper--playlist' : null
                }, [
                    vnode.attrs.playlist ?
                        m(m.route.Link, {
                            href: vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : '/watch/search/' + vnode.attrs.video.videoId, selector: 'a.video-item-stat',
                        }, vnode.attrs.video.channelTittle) :
                        [
                            m(m.route.Link, { href: vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : '/watch/search/' + vnode.attrs.video.videoId, selector: 'a.video-item-stat', class: searchModel.showThumbnail ? null : 'video-item-stat--textMode' }, vnode.attrs.video.channelTittle),
                            m('span.video-item-separator', '‧'),
                            m(m.route.Link, { href: vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : '/watch/search/' + vnode.attrs.video.videoId, selector: 'a.video-item-stat', class: searchModel.showThumbnail ? null : 'video-item-stat--textMode' }, approx(vnode.attrs.video.viewCount, { capital: true, separator: '.' }) + ' views'),
                            m('span.video-item-separator', '‧'),
                            m(m.route.Link, { href: vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : '/watch/search/' + vnode.attrs.video.videoId, selector: 'a.video-item-stat', class: searchModel.showThumbnail ? null : 'video-item-stat--textMode' }, timeAgo.ago(vnode.attrs.video.publishedAt))
                        ]
                ]),
                vnode.attrs.playlist ? null : m(m.route.Link, { href: vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : '/watch/search/' + vnode.attrs.video.videoId, selector: 'a.video-item-description', class: searchModel.showThumbnail ? null : 'video-item-description--textMode' }, m.trust(vnode.attrs.video.description)),
                vnode.attrs.playlist ? null : m(m.route.Link, { href: vnode.attrs.playlist ? '/watch/' + playlistModel.activePlaylist + '/' + vnode.attrs.video.videoId : '/watch/search/' + vnode.attrs.video.videoId, selector: 'a.video-item-durationText', class: searchModel.showThumbnail ? 'video-item-durationText--hide' : null }, 'Video duration : ' + timeFormat.fromMs(this.convertYoutubeDuration(vnode.attrs.video.duration)))
            ])
        ])
    }
}