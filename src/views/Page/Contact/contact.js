var m = require('mithril');
var madeByView = require('../../component/madeby');

module.exports = {
    oncreate: function(){
        window.scrollTo(0,0);

    },
    view: function(){
        return m('.about-body', [
            m('h1.typho__h1', 'Contact Me'),
            m('p.typho__p', 'If you want to supply bug reports, feature suggestion or talk with me in general please fill this form.'),
            m('form.contact__form', {action: 'https://formcarry.com/s/-YBk4JeXXcL', method:'POST', "accept-charset":"UTF-8"}, [
                m('h2.contact__header', 'Your email (required)'),
                m('input.contact__input', {required: true, type: 'email', name:'email'}),
                m('h2.contact__header', 'Subject (required)'),
                m('input.contact__input', {required: true, name: 'subject'}),
                m('h2.contact__header', 'Message (required)'),
                m('textarea.contact__textarea', {required: true, rows:'10', name:'message'}),
                m('button.contact__submit', {type: 'submit'}, 'Send Message')
            ]),

            m(madeByView)










        ])
    }
}