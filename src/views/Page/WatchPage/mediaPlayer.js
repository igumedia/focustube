var m = require('mithril');
var iframePlayerView = require('../../component/iframePlayer');
var playerModel = require('../../../model/playerModel').Player;

module.exports = {
    onbeforeupdate: function(){

        // if current playing video (iframe) is the same with videoId route then don't update the DOM (dont redraw)
        var shouldUpdate
        if(m.route.param().videoId === playerModel.activeVideo){
            shouldUpdate = false
        }else{
            shouldUpdate = true
        }

        return shouldUpdate
    },
    view: function(){
        return m('.watch-media__player', [
            m(iframePlayerView, {
                key: m.route.param().videoId
            })
        ])
    }
}