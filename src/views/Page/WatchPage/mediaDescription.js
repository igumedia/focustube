var m = require('mithril');
var playerModel = require('../../../model/playerModel').Player;
var autolinks = require('autolinks');
var numberSeparator = require("number-separator");
var dateFormat = require('dateformat');
var playerModel = require('../../../model/playerModel').Player

module.exports = {
    view: function(vnode){
        return m('.watch-info__description',  [
                m('.watch-info__description-tittle', m.route.param().list === 'search' ? m.trust(vnode.attrs.desc.snippet.title) : m.trust(vnode.attrs.desc.tittle)),
                m('.watch-info__description-meta', [
                     m('.watch-info__description-meta__item', m.route.param().list === 'search' ? vnode.attrs.desc.snippet.channelTitle : vnode.attrs.desc.channelTittle ),
                     m('span.video-item-separator', '‧'),
                     m('.watch-info__description-meta__item', m.route.param().list === 'search' ? numberSeparator(vnode.attrs.desc.statistics.viewCount, '.') + ' Views' :  numberSeparator(vnode.attrs.desc.viewCount, '.') + ' Views'),
                     m('span.video-item-separator', '‧'),
                     m('.watch-info__description-meta__item', m.route.param().list === 'search' ? dateFormat(vnode.attrs.desc.snippet.publishedAt, 'dd mmmm yyyy') : dateFormat(vnode.attrs.desc.publishedAt, 'dd mmmm yyyy') ),
                ]),
                m('.watch-info__description__description', {class: playerModel.showLess ? 'watch-info__description__description--hide' : null}, m.route.param().list === 'search' ?  m.trust(autolinks( vnode.attrs.desc.snippet.fullDescription)) : m.trust(autolinks( vnode.attrs.desc.fullDescription))),
                m('.watch-info__description__button', {
                    class: playerModel.showLess ? 'watch-info__description__button--hide' : null,
                    onclick: function(){
                        playerModel.toggleShowLess()
                    }
                }, playerModel.showLess ? 'SHOW MORE' : 'SHOW LESS')
            ])
    }
}
