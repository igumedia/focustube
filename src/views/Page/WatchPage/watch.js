var m = require('mithril');
var topbarView = require('../../component/topbar');
var footerView = require('../../component/footer');
var mediaPlayerView = require('./mediaPlayer');
var playlistView = require('../../component/playlist');
var mediaDescriptionView = require('./mediaDescription');
var donationView = require('../../component/donation');
var playerModel = require('../../../model/playerModel').Player;
var descriptionWrapper = require('./descriptionWrapper');
var taskTrackerView = require('../../component/taskTracker');

module.exports = {
    oncreate: function(){
        window.scrollTo(0,0);

    },
    view: function(){
        return [
            m(topbarView),
            m('.watch-body', [
                m('.watch-media', [
                    // youtube player
                    m('.watch-media__main',[
                        m('.watch-media__task', [
                            m(taskTrackerView)
                        ]),
                        m(mediaPlayerView),
                    ]),
                    // playlist
                    m('.watch-media__playlist', [
                        m(playlistView, {watchRoute : true})                 
                    ])
                    
                ]),
                // description
                [m(descriptionWrapper, {key: m.route.param().list + '__' + m.route.param().videoId})]
            ]),
            m(footerView)
        ]
    }
}