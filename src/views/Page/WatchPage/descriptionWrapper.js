var m = require('mithril');
var playerModel = require('../../../model/playerModel').Player;
var mediaDescriptionView = require('./mediaDescription');
var donationView = require('../../component/donation');

module.exports = {
    oninit: function(){
        // when component recreated call description
        playerModel.playNowDescriptionLoading = true
        playerModel.getDescription()
    },
    view: function(){
        return m('.watch-info',[
                [
                    playerModel.playNowDescriptionLoading ? m('div', 'Loading') :
                    m(mediaDescriptionView, {desc: playerModel.playNowDescription[0]})
                ],
                m('.watch-info__sidebar', [
                    m(donationView)
                ])
            ])
    }
}