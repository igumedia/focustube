var m = require('mithril');
var madeByView = require('../../component/madeby');

module.exports = {
    oncreate: function(){
        window.scrollTo(0,0);

    },
    view: function(){
        return m('.about-body', [
            m('h1.typho__h1', 'About FocusHub'),
            m('h2.typho__h2', 'What is FocusHub?'),
            m('p.typho__p', 'Do you ever watch videos on youtube to listen to relaxing music for study but ended up watching the "Cute cat compilation 2013" video?  At that point, you know you have been distracted from your initial goal.'),
            m('p.typho__p', 'FocusHub made to allow people to watch youtube videos in the less distractive environment. FocusHub omits suggestions, related videos, comment sections, and provides toggleable thumbnails with hopes users will less distracted to get things done.'),
            m('p.typho__p', 'FocusHub also provides a task tracker to reminds you of things you should do. No more  "one more video"!'),
            m('h2.typho__h2', 'For your information, FocusHub currently is in the Alpha phase.'),
            m('p.typho__p', 'FocusHub currently in the Alpha phase, which means this web app is not finished but functional enough to be used. There are some core features not released yet. This also indicates there might be a backward-incompatible update in the future.'),
            m('h2.typho__h2', 'Is FocusHub affiliated or endorsed by Youtube?'),
            m('p.typho__p', 'Nope, not at all.'),
            m('h2.typho__h2', 'Is FocusHub free to use?'),
            m('p.typho__p', 'Yes.'),
            m('h2.typho__h2', 'What is the "FocusHub Playlist"?'),
            m('p.typho__p', "FocusHub Playlist is a feature that focushub provides to make you able to queue videos.  Currently, the FocusHub Playlist is not integrated with Youtube's playlist.  FocusHub playlist stores data in the machine you use, so you cannot access a FocusHub Playlist you made in one machine from another machine."),
            m('h2.typho__h2', 'Are there any limitations?'),
            m('p.typho__p', 'Yes,  Youtube implements quota for third party software to use their API. So if FocusHub exceeds daily quota limits, FocusHub service will unusable until our quota resets the next day.'),
            m('p.typho__p', 'If you receive some error that says "Something is wrong" it might be because we have reached our quota limit.'),
            m('h2.typho__h2', 'Why FocusHub use .org domain? Is FocusHub made by an organization?'),
            m('p.typho__p', 'No, FocusHub made by an individual for fun. Initially, I want to use .com or .net but they have taken.'),
            m('h2.typho__h2', 'How can I contact you?'),
            m('p.typho__p', 'If you want to supply bug reports, feature suggestion or talk with me in general, drop me a line ', [
                m(m.route.Link, {selector: 'a', class: 'typho__a', href: '/contact'}, 'here.' )
            ]),
            m('h2.typho__h2', 'Just in case you wondering how to support me.'),
            m('p.typho__p', 'Initially, I build focushub for myself which oftentimes easily distracted by video suggestions. I spend my free time to build focushub as I enjoy building things.  At this phase, FocusHub is sufficient enough to fulfill my needs even though I have some plans to be implemented in the future.'),
            m('p.typho__p', 'Because FocusHub is not my priority  I may rarely give FocusHub an update/improvement. But if a lot of people think FocusHub is useful and necessary i may put FocusHub on my priority list.'),
            m('p.typho__p', 'So if you want to support FocusHub please use FocusHub regularly and tell me what you think about it, also it will be very helpful if you spread the word about FocusHub. If you have an extra penny,  it would be big moral support if you could ', [
                m('a.typho__a', {href:'https://www.buymeacoffee.com/migumelar'}, 'buy me a coffee'),
                m('span.typho__p', ' to keep me caffeinated. Much Thanks.'),

            ]),
            m(madeByView)










        ])
    }
}