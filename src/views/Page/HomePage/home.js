var m = require('mithril');
var footerView = require('../../component/footer')
var searchModel = require('../../../model/searchModel').Search;
var taskTrackerView = require('../../component/taskTracker');
var taskModel =require('../../../model/taskModel').Task;

module.exports = {
    view: function(){
        return [
            // m('.home-topbar'),
            m('.home-body', [
                m('.home-body__main',[
                    m('.home-body__main-topsection', [
                        m('h1.home-body__main-logo',[
                            m('span.logo__focus.logo__focus--l', 'focus'),
                            m('span.logo__tube.logo__tube--l', 'hub')
                        ]),
                        m('h2.home-body__main-jargon', 'Watch Youtube videos with less distractions to get you focus on your goals'),
                        m('.home-body__main-task', m(taskTrackerView)),
                        m('a',{class: taskModel.taskBag.length ? null : 'home-body__main-scroll--hide'}, [

                            m('a.home-body__main-scroll.fas.fa-angle-double-down.faa-float.animated', {
                                onclick: function(){
                                    // window.scrollTo(0,document.body.scrollHeight)
                                    document.getElementById("search_desc").scrollIntoView();

                                },  
                                tittle: 'Jump to search bar',
                                class: taskModel.taskBag.length ? null : 'home-body__main-scroll--hide',
                            })
                        ])
                    ]),

                    m('form.home-body__main-search',{
                        id: 'search_form',
                        class: taskModel.taskBag.length ? null : 'home-body__main-search--hide',
                        onsubmit: function(e){
                            searchModel.redirectSearch(e, '');
                        }
                    }, [
                        m('h2.home-body__main-jargon', {id: 'search_desc'} ,"Let's watch some videos, but remember to focus on your goals!"),
                        m('.home-body__main-inputwrapper', [
                            m('input.home-body__main-search__input', {
                                id: 'search',
                                placeholder: 'Search Youtube videos...', 
                                required : true,
                                oninput: function(e){
                                    searchModel.setKeyword(e.target.value)
                                }
                            }),
                            m('button.home-body__main-search__button', {type: 'submit'}, [
                                m('.home-body__main-search__button__search.i.fas.fa-search')
                            ])
                        ])
                    ])
                ])
            ]),
            m(footerView)
        ]
    }
}