var m = require('mithril');
var filterView = require('./filter')
var videoItemView = require('../../component/videoItem');
var paginationView = require('./pagination');
var playlist = require('../../component/playlist');
var searchModel = require('../../../model/searchModel').Search;
var taskTrackerView = require('../../component/taskTracker');
var uniqid = require('uniqid');



module.exports = {
    oninit:function(){
        if(!searchModel.keyword){
            m.route.set('/')
        }
    },
    view: function(){
        return m('.search-body', [
            m('.search-container', {class: searchModel.showPlaylist? null : 'search-container--full' }, [
                m('.search-result', [
                    m(taskTrackerView),
                    m(filterView),
                    m('.search-result-container', [
                        searchModel.searchLoading ? m('div', 'Loading search result') :
                        searchModel.searchResult['items'].length === 0 ? m('div', 'Nothing found') :
                        searchModel.searchResult['items'].map(function(result){
                            return m(videoItemView, {
                                tes: 'tes',
                                // videoId: result.id.videoId,
                                // publishedAt: result.snippet.publishedAt,
                                // tittle: result.snippet.title,
                                // channelTittle: result.snippet.channelTitle,
                                // thumbnail: result.snippet.thumbnails.medium.url,
                                // description: result.snippet.description,
                                // duration: result.contentDetails.duration,
                                // viewCount: result.statistics.viewCount,
                                // fullDescription: result.snippet.fullDescription
                                video : {
                                    uniqId: uniqid(),
                                    createdAt: new Date().toISOString(),
                                    videoId: result.id.videoId,
                                    publishedAt: result.snippet.publishedAt,
                                    tittle: result.snippet.title,
                                    channelTittle: result.snippet.channelTitle,
                                    thumbnail: result.snippet.thumbnails.medium.url,
                                    description: result.snippet.description,
                                    duration: result.contentDetails.duration,
                                    viewCount: result.statistics.viewCount,
                                    fullDescription: result.snippet.fullDescription
                                }
                            })
                        })
                    ]),
                    m(paginationView, {pageToken : searchModel.searchResult})
                ])
            ]),
            m('.search-playlist',{class: searchModel.showPlaylist? null : 'search-playlist--hide' } ,[
                m(playlist)
            ])
        ])
    }
}