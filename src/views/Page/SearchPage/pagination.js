var m = require('mithril');
var searchModel = require('../../../model/searchModel').Search;

module.exports = {
    view: function(vnode){
        return m('.search-pagination', [
            m('a.search-pagination__direction', {
                class: vnode.attrs.pageToken.prevPageToken ? null : 'search-pagination__direction--hide',
                onclick: function(e){
                    searchModel.redirectSearch(e, vnode.attrs.pageToken.prevPageToken)
                }
            }, [
                m('i.search-pagination__direction-icon.fas.fa-chevron-left'),
                m('.search-pagination__direction-label', 'Previous')
            ]),
            m('a.search-pagination__direction', {
                class: vnode.attrs.pageToken.nextPageToken ? null : 'search-pagination__direction--hide',
                onclick: function(e){
                    searchModel.redirectSearch(e, vnode.attrs.pageToken.nextPageToken)
                }
            }, [
                m('.search-pagination__direction-label', 'Next'),
                m('i.search-pagination__direction-icon.fas.fa-chevron-right')
            ])
        ])
    }
}