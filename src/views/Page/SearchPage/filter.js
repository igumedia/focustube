var m = require('mithril');
var searchModel = require('../../../model/searchModel').Search

module.exports = {
    view: function(){
        return m('.search-filter', [
            m('.search-filter-header', [
                m('.search-filter-header__filter', {onclick: searchModel.setFilter} ,[
                    m('i.search-filter-header__filter-icon.fas.fa-cog'),
                    m('.search-filter-header__filter-text', 'Options')
                ]),
                m('.search-filter-header__control', [
                    m('.search-filter-header__control-option', [
                        m('.search-filter-header__control-text', 'Show thumbnail'),
                        m('select.search-filter-header__control-select',{name: 'toggleThumbnail', onchange:searchModel.setThumbnail},[
                            m('option',{value: true, selected : searchModel.showThumbnail ? true : null },'Yes'),
                            m('option',{value: false, selected : searchModel.showThumbnail ? false : true},'No')
                        ])
                    ]),
                    m('.search-filter-header__control-option', [
                        m('.search-filter-header__control-text', 'Show playlist'),
                        m('select.search-filter-header__control-select',{name: 'togglePlaylist', onchange:searchModel.setPlaylist},[
                            m('option',{value: true, selected : searchModel.showPlaylist ? true : null },'Yes'),
                            m('option',{value: false, selected : searchModel.showPlaylist ? false : true},'No')
                        ])
                    ])
                ])
            ]),
            m('.search-filter-body',{class: searchModel.showFilter ? null: 'search-filter-body--hide' },[
                m('.search-filter-body__field', [
                    m('.search-filter-body__field-label', 'Search for'),
                    m('.search-filter-body__field-radio', [
                        m('input', {type: 'radio', name:'type', checked: true, value:'video', id:'video'}),
                        m('label', {for: 'video'}, 'Video'),
                        m('input', {type: 'radio', name:'type', disabled: true, value:'playlist', id:'playlist'}),
                        m('label', {for: 'playlist'}, 'Playlist (Planned)')
                    ])
                ]),
                m('.search-filter-body__field', [
                    m('.search-filter-body__field-label', 'Sort by'),
                    m('.search-filter-body__field-radio', {
                        onchange: function(e){
                            searchModel.setSortBy(e.target.value)
                        }
                    }, [
                        m('input', {type: 'radio', name:'sortBy', value:'relevance', id:'relevance', checked: searchModel.sortBy === 'relevance' ? true : null}),
                        m('label', {for: 'relevance'}, 'Relevance'),
                        m('input', {type: 'radio', name:'sortBy', value:'date', id:'date', checked: searchModel.sortBy === 'date' ? true : null}),
                        m('label', {for: 'date'}, 'Upload date'),
                        m('input', {type: 'radio', name:'sortBy', value:'viewCount', id:'viewCount', checked: searchModel.sortBy === 'sortBy' ? true : null}),
                        m('label', {for: 'viewCount'}, 'View count')
                    ])
                ])
            ])

        ])
    }
}