var m = require('mithril');
var topbarView = require('../../component/topbar');
var footerView = require('../../component/footer');
var searchBodyView = require('./searchBody');


module.exports = {
    oncreate: function(){
        window.scrollTo(0,0);

    },
    view: function(){
        return [
            m(topbarView),
            m(searchBodyView),
            m(footerView)
        ]
    }
}