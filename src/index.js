var m = require('mithril');
var wrapperView = require('./views/component/wrapper');
var homeView = require('./views/Page/HomePage/home');
var searchView = require('./views/Page/SearchPage/search')
var watchView = require('./views/Page/WatchPage/watch');
var promptView = require('./views/component/prompt');
var topBarView = require('./views/component/topbar');
var aboutBodyView = require('./views/Page/AboutPage/about');
var termsAndPolicyView = require('./views/Page/TermsAndPolicyPage/termsAndPolicy');
var contactView = require('./views/Page/Contact/contact');

var footerView = require('./views/component/footer');

m.route(document.body, '/', {
    '/' : {
        render: function(){
            return m('.home', m(homeView))
        }
    },
    '/about': {
        render: function(){
            return m('.about', [
                m(topBarView),
                m(aboutBodyView),
                m(footerView)
            ])
        }
    },
    '/terms-and-policy': {
        render: function(){
            return m('.about', [
                m(topBarView),
                m(termsAndPolicyView),
                m(footerView)
            ])
        }
    },
    '/contact': {
        render: function(){
            return m('.about', [
                m(topBarView),
                m(contactView),
                m(footerView)
            ])
        }
    },
    '/search' : {
        render: function(){
            return m(wrapperView, [
                m(searchView),
                m(promptView)
            ])
        }
    },
    '/watch/:list/:videoId' : {
        render: function(){
            return m(wrapperView,[
                // m(watchView, {key: m.route.param().videoId})
                m(watchView),
                m(promptView)
            ] )
        }
    },   

})