var m = require('mithril')
var uniqId = require('uniqid')
var localforage = require('localforage')
var baseModel = require('./baseModel').Base


var Playlist = {
    thumbnailMode : false,
    textMode : true,
    shuffle: false,
    showBody: true,
    activePlaylist: 'temporary',
    temporary : [],
    list:{},
    toggleBody: function(){
        Playlist.showBody = !Playlist.showBody
    },
    toggleShuffle: function(){
        Playlist.shuffle = !Playlist.shuffle
    },
    initialLoad : function(){


        baseModel.focushub_playlist_version_control.getItem('version').then(function(value){
            if(value === baseModel.focushub__playlist__version){
                // if the offline storage scheme is latest version

                // load state from indexDB
                baseModel.focushub_playlist.iterate(function(value, key, iterationNumber) {
                    if(key != 'temporary'){
                        Playlist.list[key] = value
                    }else{
                        Playlist.temporary = value
                    }
                    m.redraw()
                    // return true
                }).catch(function(err) {
                    // This code runs if there were any errors
                alert('Sorry something is wrong')
                });

            }else{
                baseModel.focushub_playlist.clear().then(function() {
                    // Run this code once the database has been entirely deleted.
                    baseModel.focushub_playlist_version_control.setItem('version', baseModel.focushub__task__version).then(function(value){
                        console.info('Playlist offline storage version is not the same with latest release version, your offline storage is cleared.')
                    })
                }).catch(function(err) {
                    // This code runs if there were any errors
                    console.log(err);
                });
            }
        }).catch(function(err) {
            alert('Unable to connect with offline storage')
        });


















    },
    setActivePlaylist: function(value){
        Playlist.activePlaylist = value
    },
    saveNewPlaylist: function(title){
        
        var playlistId = uniqId('playlist_')

        // save local object
        Playlist.list[playlistId] = {
            playlist_name: title,
            playlist_videos: Playlist.temporary
        }
        Playlist.temporary = []

        // save to localforage
        baseModel.focushub_playlist.setItem(playlistId, Playlist.list[playlistId]).catch(function(err) {
            alert('Sorry something is wrong')
        })

        baseModel.focushub_playlist.setItem('temporary', []).catch(function(err) {
            alert('Sorry something is wrong')
        })

        // set active playlist and redirect to current playlist (url) if it's on watch page
        Playlist.activePlaylist = playlistId
        if(m.route.param().list === 'temporary'){
            m.route.set('/watch/' + playlistId + '/' + m.route.param().videoId )
        }
     
    },
    toggleMode : function(){
        Playlist.thumbnailMode = !Playlist.thumbnailMode
        Playlist.textMode = !Playlist.textMode
    },
    removePlaylistVideo: function(uniqId){


        // remove video from temporary playlist
        if(Playlist.activePlaylist === 'temporary'){
            for( var i = 0; i < Playlist.temporary.length; i++){ 
                if ( Playlist.temporary[i].uniqId === uniqId) {
                    Playlist.temporary.splice(i, 1); 
                }
             }

            // store to indexedDB
            baseModel.focushub_playlist.setItem('temporary', Playlist.temporary).catch(function(err) {
                alert('Sorry something is wrong')
            })

            return
        }else{

            for( var i = 0; i < Playlist.list[Playlist.activePlaylist].playlist_videos.length; i++){ 
                if ( Playlist.list[Playlist.activePlaylist].playlist_videos[i].uniqId === uniqId) {
                    Playlist.list[Playlist.activePlaylist].playlist_videos.splice(i, 1); 
                }
             }

            // store to indexedDB
            baseModel.focushub_playlist.setItem(Playlist.activePlaylist, Playlist.list[Playlist.activePlaylist]).catch(function(err) {
                alert('Sorry something is wrong')
            })
             return
        }



    },
    insertPlaylistVideo : function(video){

        // insert video to temporary playlist
        if(Playlist.activePlaylist === 'temporary'){
            Playlist.temporary.push(video)

            // store to indexedDB
            baseModel.focushub_playlist.setItem('temporary', Playlist.temporary).catch(function(err) {
                alert('Sorry something is wrong')
            })
    
        }else{

            Playlist.list[Playlist.activePlaylist].playlist_videos.push(video)
            // store to indexedDB
            baseModel.focushub_playlist.setItem(Playlist.activePlaylist, Playlist.list[Playlist.activePlaylist]).catch(function(err) {
                alert('Sorry something is wrong')
            })
        }
    },
    deletePlaylist : function(){

        // delete temporary playlist
        if(Playlist.activePlaylist === 'temporary'){
            Playlist.temporary = []

            // store to indexedDB
            baseModel.focushub_playlist.setItem('temporary', Playlist.temporary).catch(function(err) {
                alert('Sorry something is wrong')
            })

        }else{


            if (Playlist.activePlaylist === m.route.param().list){
                alert('Sorry you cannot delete a playing playlist.')
            }else{

                var deletedPlaylist = Playlist.activePlaylist
                Playlist.activePlaylist = 'temporary'
    
                // delete named playlist
                delete Playlist.list[deletedPlaylist]
    
                // store to indexedDB
                baseModel.focushub_playlist.removeItem(deletedPlaylist).catch(function(err) {
                    alert('Sorry something is wrong')
                })

            }


        }
    }
}

module.exports = {
    Playlist
}