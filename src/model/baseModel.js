var m = require('mithril')
var localforage = require('localforage')


var Base = {
    key: 'AIzaSyDQfU-UqngMyygOzJQAk6rZ_NgzABnNWLo',
    searchMaxResult: '12',
    focushub__task__version: '1.0.0-alpha',
    focushub__playlist__version: '1.0.0-alpha',
    focushub_playlist_version_control: localforage.createInstance({
        name: "focushub_playlist_version_control"
    }),
    focushub_playlist: localforage.createInstance({
        name: "focushub_playlist"
    }),
    focushub_task: localforage.createInstance({
        name: "focushub__task"
    })
}

module.exports = {
    Base
}