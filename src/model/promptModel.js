var m = require('mithril')

var Prompt = {
    showPrompt: false,
    headerPrompt: '',
    descriptionPrompt: '',
    inputPrompt: false,
    valueInputPrompt: true,
    callbackPrompt: '',
    togglePrompt: function(header, description, input, callback){
        
        Prompt.headerPrompt = header;
        Prompt.descriptionPrompt = description;
        Prompt.inputPrompt = input;
        Prompt.valueInputPrompt = ''

        if(callback){
            Prompt.callbackPrompt = callback
        }

        
        // Pop up the prompt
        Prompt.showPrompt = true;
    },
    setInput: function(value){
        Prompt.valueInputPrompt = value;
    },
    submitPrompt: function(e){
        e.preventDefault()
        // console.log(Prompt.valueInputPrompt)
        // console.log(Prompt.callbackPrompt)
        Prompt.callbackPrompt(Prompt.valueInputPrompt)
        Prompt.resetPrompt()
    },
    resetPrompt: function(){
        Prompt.showPrompt= false;
        Prompt.headerPrompt= '';
        Prompt.descriptionPrompt= '';
        Prompt.inputPrompt= false;
        Prompt.valueInputPrompt= true;

        return
    }

}

module.exports = {
    Prompt
}