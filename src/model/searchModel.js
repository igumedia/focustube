var m = require('mithril')
var baseModel = require('./baseModel').Base

var Search = {
    searchResult: [],
    searchLoading: true,
    keyword: '',
    type: 'video',
    showFilter: false,
    showThumbnail: false,
    showPlaylist: true,
    searchFor: 'video',
    sortBy: 'relevance',
    setSortBy: function (value) {
        Search.sortBy = value
    },
    setKeyword: function (value) {
        Search.keyword = value
    },
    setFilter: function () {
        Search.showFilter = !Search.showFilter
    },
    setThumbnail: function () {
        Search.showThumbnail = !Search.showThumbnail
    },
    setPlaylist: function () {
        Search.showPlaylist = !Search.showPlaylist
    },
    redirectSearch: function (e, token) {
        e.preventDefault(e)
        var pageToken
        if (token) {
            pageToken = '&page_token=' + encodeURI(token)
        } else {
            pageToken = ''
        }

        m.route.set('/search?search_query=' + encodeURI(Search.keyword) + pageToken)
        Search.goSearch()

    },
    goSearch: function () {

        var parseQuery = window.location.toString().substring(window.location.toString().lastIndexOf('?'))
        var parseQuery = m.parseQueryString(parseQuery)

        var maxResults = baseModel.searchMaxResult
        var sortBy = Search.sortBy
        var key = baseModel.key
        var query = parseQuery.search_query

        // console.log(parseQuery)

        var pageToken
        if (parseQuery.page_token) {
            pageToken = '&pageToken=' + parseQuery.page_token
        } else {
            pageToken = ''
        }

        var url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=' + maxResults + '&order=' + sortBy + '&q=' + query + '&type=' + Search.searchFor + pageToken + '&key=' + key
        Search.searchLoading = true;

        // return

             m.request({
                method: "GET",
                url: url
            }).then(function (data) {

                // doing search.list api
                Search.searchLoading = false
                var bulkId = data.items.map(function (videos) {
                    return videos.id.videoId
                })

                return {
                    bulkId: bulkId,
                    searchResult: data
                }

            }).then(function (data) {
                Search.searchLoading = true;

                // do ajax call for search
                m.request({
                    method: "GET",
                    url: 'https://www.googleapis.com/youtube/v3/videos?part=statistics%2CcontentDetails%2Csnippet&id=' + encodeURIComponent(data.bulkId) + '&fields=items(contentDetails%2Fduration%2Cid%2Csnippet%2Fdescription%2Cstatistics%2FviewCount)&key=' + key
                }).then(function (videoDetail) {

                    // loop each serch result
                    for (var i = 0; i < data.searchResult.items.length; i++) {
                        // check if each have search result have same ID with videoDetail array, if it is, merge value
                        videoDetail.items.forEach(function (index) {
                            if (data.searchResult.items[i].id['videoId'] === index.id) {
                                data.searchResult.items[i].statistics = index.statistics
                                data.searchResult.items[i].contentDetails = index.contentDetails
                                data.searchResult.items[i].snippet.fullDescription = index.snippet.description
                            }
                        })
                    }

                    Search.searchResult = data.searchResult
                    Search.searchLoading = false;
                    // console.log(data.searchResult)

                    return
                }).catch(function (err) {
                    // console.log(err)
                    alert('Sorry something is wrong');
                    Search.searchLoading = false;
                })

            }).catch(function (err) {
                alert('Sorry something is wrong');
                Search.searchLoading = false;
            })
    }
}

module.exports = {
    Search
}