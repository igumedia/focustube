var m = require('mithril');
var searchModel = require('./searchModel').Search;
var baseModel = require('./baseModel').Base;
var playlistModel = require('./playlistModel').Playlist;
var YTPlayer = require('yt-player')



var Player = {
    activeVideo : '',
    playerInstance : '',
    playNowDescription : [],
    playNowDescriptionLoading: true,
    showLess : true,
    toggleShowLess: function(){
        Player.showLess = !Player.showLess
    },
    createInstance: function(){
        Player.playerInstance = new YTPlayer('#yt_player', {
            autoplay: true,
            modestBranding: true,
            fullscreen: true,
            related: false,
            info: false
        })
    },
    playVideo: function(videoId){
        Player.playerInstance.load(videoId, true)
        Player.activeVideo = videoId
        console.log('im here to be called')
        m.redraw()
    }, 
    nextVideo: function(){

        var index

        // if from search
        if(m.route.param().list === 'search' ){
            return
        }else if(m.route.param().list === 'temporary'){

            // next videos for temporary mode
            if(playlistModel.shuffle === false){
                // straight mode
                for( var i = 0; i < playlistModel.temporary.length; i++){ 
                    if ( playlistModel.temporary[i].videoId === m.route.param().videoId) {
                        index = i + 1;
                        if(index > (playlistModel.temporary.length - 1)){
                            index = 0
                        }
                        break;
                    }
                }

                if(typeof index != 'undefined'){
                    // if user not manually enter video ID on URL than play next video when video ended
                    Player.activeVideo = playlistModel.temporary[index].videoId;
                    Player.playerInstance.load(playlistModel.temporary[index].videoId, true)
                    m.route.set('/watch/temporary/' + playlistModel.temporary[index].videoId)
                    return
                }else{
                    // if user not manually enter video ID on URL than do nothing when video ended
                    return
                }
                
            }else{
                // shuffle mode
                do {
                    index = playlistModel.temporary[Math.floor(Math.random() * playlistModel.temporary.length)]
                }
                while(index.videoId === m.route.param().videoId) 

                Player.activeVideo = index.videoId;
                Player.playerInstance.load(index.videoId, true)
                m.route.set('/watch/temporary/' + index.videoId)
                return
            }

        }else{

            // next videos for playlist mode
            if(playlistModel.shuffle === false){
                // straight mode
                for( var i = 0; i < playlistModel.list[m.route.param().list].playlist_videos.length; i++){ 
                    if ( playlistModel.list[m.route.param().list].playlist_videos[i].videoId === m.route.param().videoId) {
                        index = i + 1;
                        if(index > (playlistModel.list[m.route.param().list].playlist_videos.length-1)){
                            index = 0
                        }
                        break;
                    }
                }

                if(typeof index != 'undefined'){
                    // if user not manually enter video ID on URL than play next video when video ended
                    Player.activeVideo = playlistModel.list[m.route.param().list].playlist_videos[index].videoId;
                    Player.playerInstance.load(playlistModel.list[m.route.param().list].playlist_videos[index].videoId, true)
    
                    m.route.set('/watch/' + m.route.param().list + '/' + playlistModel.list[m.route.param().list].playlist_videos[index].videoId)
                    m.redraw()
                    return
                }else{
                    // if user not manually enter video ID on URL than do nothing when video ended
                    return
                }

            }else{
                // shuffle mode
                do{
                    index = playlistModel.list[m.route.param().list].playlist_videos[Math.floor(Math.random() * playlistModel.list[m.route.param().list].playlist_videos.length)]
                }
                while(index.videoId === m.route.param().videoId) 

                Player.activeVideo = index.videoId;
                Player.playerInstance.load(index.videoId, true)
                m.route.set('/watch/' + m.route.param().list + '/' + index.videoId)
                return
            }

        }
    },
    getIndependentDescription: function(){

        var result

        m.request({
            method: "GET",
            url: 'https://www.googleapis.com/youtube/v3/videos?part=statistics%2CcontentDetails%2Csnippet&id=' + m.route.param().videoId + '&fields=items(contentDetails%2Fduration%2Cid%2Csnippet%2Fdescription%2Cstatistics%2FviewCount)&key=' + baseModel.key
        }).then(function (videoDetail) {
            
            // console.log('im caleld')
            return result = videoDetail.items

        }).catch(function (err) {
            alert('Sorry something is wrong');
        })

        return result

    },
    getSingleDescription: function(){

            // just in case user change URL manually call the description manually too on Playlist mode or temporary
            if(Player.playNowDescription.length === 0){
                // console.log('im in single description')

                m.request({
                    method: "GET",
                    url: 'https://www.googleapis.com/youtube/v3/videos?part=statistics%2Csnippet&id=' + m.route.param().videoId + '&fields=items(snippet(channelTitle%2Cdescription%2CpublishedAt%2Ctitle)%2Cstatistics%2FviewCount)&key=' + baseModel.key
                }).then(function (videoDetail) {
                    
                    Player.playNowDescription = videoDetail.items
                    Player.playNowDescription = [{
                        tittle: videoDetail.items[0].snippet.title,
                        publishedAt: videoDetail.items[0].snippet.publishedAt,
                        fullDescription: videoDetail.items[0].snippet.description,
                        channelTittle:videoDetail.items[0].snippet.channelTitle,                    
                        viewCount: videoDetail.items[0].statistics.viewCount    
                    }]

                    Player.playNowDescriptionLoading= false
                    // m.redraw()
                    // console.log(Player.playNowDescription)
                }).catch(function (err) {
                    console.log(err)
                    alert('Sorry something is wrong');
                })

            }else{
                Player.playNowDescriptionLoading= false
            }

    },
    getDescription: function(){
        // console.log('recreated')
        Player.playNowDescriptionLoading = true
        Player.playNowDescription = []

        // console.log(m.route.param().list)

        if(m.route.param().list === 'search'){
            // if source from search then fetch description from search state   
            if(searchModel.searchResult.items){
                searchModel.searchResult.items.forEach(function(i){
                    if(m.route.param().videoId === i.id.videoId){
                        Player.playNowDescription.push(i)
                    }
                })   
                Player.playNowDescriptionLoading= false             
            }



            // just in case user change URL manually call the description manually too
            if(Player.playNowDescription.length === 0){


                m.request({
                    method: "GET",
                    url: 'https://www.googleapis.com/youtube/v3/videos?part=statistics%2Csnippet&id=' + m.route.param().videoId + '&fields=items(snippet(channelTitle%2Cdescription%2CpublishedAt%2Ctitle)%2Cstatistics%2FviewCount)&key=' + baseModel.key
                }).then(function (videoDetail) {
                    
                    Player.playNowDescription = videoDetail.items
                    Player.playNowDescription = [{
                        snippet: {
                            title: videoDetail.items[0].snippet.title,
                            publishedAt: videoDetail.items[0].snippet.publishedAt,
                            fullDescription: videoDetail.items[0].snippet.description,
                            channelTitle:videoDetail.items[0].snippet.channelTitle
                        },
                        statistics: {
                            viewCount: videoDetail.items[0].statistics.viewCount
                        }
                    }]
                    Player.playNowDescriptionLoading= false

                    // console.log(Player.playNowDescription)
                }).catch(function (err) {
                    // console.log(err)
                    alert('Sorry something is wrong');
                })
            }

            // Player.playNowDescriptionLoading= false
            // m.redraw()
            // return
    

        }else if(m.route.param().list === 'temporary'){
            // console.log('im in temporary')
            // if source from temporary list then fetch description from search temporary
            if(playlistModel.temporary){
                playlistModel.temporary.forEach(function(i){
                    if(m.route.param().videoId === i.videoId){
                        Player.playNowDescription.push(i)
                    }
                })                
            }

            // console.log(Player.playNowDescription)

            Player.getSingleDescription()
            // Player.playNowDescriptionLoading= false
            // m.redraw()
            // return
    

        }else{
            
            // if source from temporary list then fetch description from search temporary
            if(playlistModel.list[m.route.param().list]){
                playlistModel.list[m.route.param().list].playlist_videos.forEach(function(i){
                    if(m.route.param().videoId === i.videoId){
                        Player.playNowDescription.push(i)
                    }
                })                
            }

            // console.log(Player.playNowDescription)

            Player.getSingleDescription()
            // Player.playNowDescriptionLoading= false
            // m.redraw()
            // return
    
        }


    }
}


module.exports = {
    Player
}