var m = require('mithril')
var uniqId = require('uniqid')
var baseModel = require('../model/baseModel').Base;

var Task = {
    taskBag : [],
    taskInput: '',
    taskView: 'all',
    hideBrowser: false,
    toggleHideBrowser: function(){
        Task.hideBrowser = !Task.hideBrowser
    },
    storeToLocal: function(){
        baseModel.focushub_task.setItem('taskBag', Task.taskBag).catch(function(err){
            alert('Failed storing task to local storage')
            console.log(err)
        })
    },
    initialLoad: function(){

        baseModel.focushub_task.getItem('version').then(function(value){
            if(value === baseModel.focushub__task__version){
                // put taskBag from localforage to current variable
                baseModel.focushub_task.getItem('taskBag').then(function(value) {
                    // This code runs once the value has been loaded
                    // from the offline store.

                    if(value){
                        Task.taskBag = value
                    }
                    m.redraw()
                }).catch(function(err) {
                    // This code runs if there were any errors
                    alert('Failed to load task from offline storage')
                    console.log(err);
                });
                
            }else{
                baseModel.focushub_task.clear().then(function() {
                    // Run this code once the database has been entirely deleted.
                    baseModel.focushub_task.setItem('version', baseModel.focushub__task__version).then(function(value){
                        console.info('Task tracker offline storage version is not the same with latest release version, your offline storage is cleared.')
                    })
                }).catch(function(err) {
                    // This code runs if there were any errors
                    console.log(err);
                });
            }
        }).catch(function(err) {
            alert('Unable to connect with offline storage')
        });
        
        
    },
    setTaskInput: function(value){
        Task.taskInput = value
    },
    setTaskView: function(value){
        Task.taskView = value
    },
    setExpandDetails: function(task){
        task.expandDetails = !task.expandDetails
    },
    setCompleted: function(task){
        var status = !task.completed
        task.completed = status

        if(status === true){
            var completedDate = new Date().toISOString() 
            task.completed = true
            task.completedAt = completedDate
            task.TTC = Math.abs(new Date(completedDate) - new Date(task.createdAt))

        }else{
            task.completed = false
            task.completedAt = false
            task.TTC = false
        }

        Task.storeToLocal()
    },
    removeTask: function(taskId){
        for( var i = 0; i < Task.taskBag.length; i++){ 
            if ( Task.taskBag[i].taskId === taskId) {
                Task.taskBag.splice(i, 1); 
            }
         }

         Task.storeToLocal()
    },
    submitTask: function(){

        if(Task.taskInput.trim()){
            var taskId = uniqId('task_')
            Task.taskBag.push({
                taskId : taskId,
                expandDetails: false,
                completed: false,
                createdAt: new Date().toISOString(),
                completedAt: false,
                TTC: false,
                taskDesc: Task.taskInput,
            })
    
            Task.storeToLocal()
        }

        Task.taskInput = ''

    },
       
}

module.exports = {
    Task
}